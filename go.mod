module svarmr

go 1.23.0

require (
	github.com/andlabs/ui v0.0.0-20200610043537-70a69d6ae31e
	github.com/getlantern/systray v1.2.2
	github.com/hashicorp/mdns v1.0.6
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/oleksandr/bonjour v0.0.0-20210301155756-30f43c61b915
	github.com/tiborvass/uniline v0.0.0-20150528164013-b39ee5b15151
	gitlab.com/donomii/svarmrgo v0.0.0-20210405175128-4ca3ce55695c
	golang.org/x/net v0.35.0
)

require (
	github.com/getlantern/context v0.0.0-20190109183933-c447772a6520 // indirect
	github.com/getlantern/errors v0.0.0-20190325191628-abdb3e3e36f7 // indirect
	github.com/getlantern/golog v0.0.0-20190830074920-4ef2e798c2d7 // indirect
	github.com/getlantern/hex v0.0.0-20190417191902-c6586a6fe0b7 // indirect
	github.com/getlantern/hidden v0.0.0-20190325191715-f02dbb02be55 // indirect
	github.com/getlantern/ops v0.0.0-20190325191751-d70cb0d6f85f // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/miekg/dns v1.1.55 // indirect
	github.com/oxtoacart/bpool v0.0.0-20190530202638-03653db5a59c // indirect
	github.com/shinichy/go-wcwidth v0.0.0-20140219061058-b202ee861e25 // indirect
	golang.org/x/crypto v0.33.0 // indirect
	golang.org/x/mod v0.17.0 // indirect
	golang.org/x/sync v0.10.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	golang.org/x/term v0.29.0 // indirect
	golang.org/x/tools v0.21.1-0.20240508182429-e35e4ccd0d2d // indirect
)
