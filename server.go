//The message hub.  Server relays messages between programs
//
//Svarmr now works exclusively over STDIN/STDOUT for security.  There are TCP connector
//modules if you want to link svarmr across multiple computers.
//
//Start the server with:
//
//  server modulename modulename modulename
package main

//This forces go get to download the required modules
import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"

	"github.com/kardianos/osext"
	"gitlab.com/donomii/svarmrgo"
)

const queueLength int = 1

var SvarmrDirectory string
var AppDirectory string
var inMessages int = 0
var outMessages int = 0

var connList []net.Conn
var subprocList []*subProx

type connection struct {
	port   net.Conn
	handle *subProx
	raw    string
}

type subProx struct {
	In   io.WriteCloser
	Out  io.ReadCloser
	Err  io.ReadCloser
	Cmd  *exec.Cmd
	Name string
}

//Read incoming messages and place them on the input queue
func handleSubprocConnection(conn *subProx, Q chan connection) {



	// Read lines from the subprocess error handle, and print them on our own error handle.  Don't add our own prefix, as it clutters the output
	ErrReader := bufio.NewReader(conn.Err)
	go func() {
		for {
			t, err := ErrReader.ReadString('\n')
			if err != nil {
				//log.Println("Client disconnected: ", err)
				return
			}
			fmt.Fprintf(os.Stderr, "%s", t)
		}
	}()

	// Read lines from the subprocess output handle, and process them
	reader := bufio.NewReader(conn.Out)
	for {
		t, err := reader.ReadString('\n')
		if err != nil {
			log.Println("Client disconnected: ", err)
			inQ <- connection{nil, nil, svarmrgo.WireFormat(svarmrgo.Message{Selector: "finish-module", Arg: fmt.Sprintf("Module disconnected: %v", err)})}
			return
		}
		var m connection = connection{nil, conn, t}
		Q <- m
	}

}

//Read incoming messages and place them on the input queue
func readMesagesFromStdin( Q chan connection) {

	reader := bufio.NewReader(os.Stdin)

	for {
		t, err := reader.ReadString('\n')
		if err != nil {
			log.Println("Client disconnected: ", err)
			inQ <- connection{nil, nil, svarmrgo.WireFormat(svarmrgo.Message{Selector: "finish-module", Arg: fmt.Sprintf("Module disconnected: %v", err)})}
			return
		}
		var m connection = connection{nil, nil, t}
		Q <- m
	}

}

// Read error messages from a subprocess, and turn them into svarmr messages
func handleSubprocErrors(conn *subProx, Q chan connection) {
	reader := bufio.NewReader(conn.Err)

	for {
		t, err := reader.ReadString('\n')
		if err != nil {
			log.Println("Client disconnected: ", err)
			inQ <- connection{nil, nil, svarmrgo.WireFormat(svarmrgo.Message{Selector: "finish-module", Arg: fmt.Sprintf("Module disconnected: %v", err)})}
			return
		}
		log.Println("Module error: ", t)
		inQ <- connection{nil, nil, svarmrgo.WireFormat(svarmrgo.Message{Selector: "error-module", Arg: "Module error: " + t})}
	}

}

//Attempts to start a svarmr module.  Because we are cross platform, we have to check for multiple types of executable - on windows, we must support .bat and .exe, on linux we must support .sh and "no extension".  The user gives us a module name, and we go through, checking each extension.
//We also have to check multiple locations for modules.  The intent is that the application developer writes their program in a separate directory, and then runs svarmr, which is installed in its own directory.  Every time a module is loaded, svarmr must check the application directory first, then the base svarmr directory.
//To do this, pass a list of paths to StartSubproc, and the paths will be checked in order
func StartSubproc(orig_cmd string, args []string, paths []string) *subProx {
	log.Println("Search paths", paths)
	for _, moduleDir := range paths {
		log.Printf("Searching for %v in %v", orig_cmd, moduleDir)
		os.Chdir(moduleDir)
		dir, err := os.Getwd() //FFS
		if err != nil {
			log.Printf("Failed to change directory to %v, we are in %v", moduleDir, dir)
			continue
		}

		//It turns out that cmd.Start() doesn't actually tell us if the subprocess started,
		//just that the internal call succeeded.  So we have no actual way of telling if the
		//subprocess started, without waiting for it to quit.
		var cmd string
		var handle *subProx
		if runtime.GOOS == "windows" {
			fullPath := fmt.Sprintf("%s\\%s.bat", moduleDir, orig_cmd)
			//ugh
			detectPath := fullPath
			cmd = strings.Replace(fullPath, "/", "\\", -1)
			log.Println("Trying ", cmd)
			if _, err := os.Stat(detectPath); !os.IsNotExist(err) {
				log.Println("Found", cmd)
				handle = ActualStartSubproc(cmd, args)
			} else {
				fullPath := fmt.Sprintf("%s\\%s.exe", moduleDir, orig_cmd)
				cmd = fullPath
				log.Println("Trying ", cmd)
				handle = ActualStartSubproc(cmd, args)
			}
			if handle != nil {
				return handle
			} else {
				log.Println("Failed")
			}
		} else {
			cmd = fmt.Sprintf("%s/%s.sh", moduleDir,orig_cmd)
			log.Println("Trying ", cmd)
			if _, err := os.Stat(cmd); !os.IsNotExist(err) {
				handle = ActualStartSubproc(cmd, args)
				return handle
			} else {
				fullPath := fmt.Sprintf("%s/%s", moduleDir, orig_cmd)
				log.Println("Trying ", fullPath)
				if _, err := os.Stat(fullPath); !os.IsNotExist(err) {
					handle := ActualStartSubproc(fullPath, args)
					if handle != nil {
						log.Println("Succeeded: ", fullPath)
						return handle
					} 
				} else {
					log.Println("File not found:", fullPath)
				}
			}
		}
	}
	log.Printf("Failed to find %v in any known directory", orig_cmd)
	return nil
}

func ActualStartSubproc(cmd string, args []string) *subProx {
	grepCmd := exec.Command(cmd, args...)

	grepIn, _ := grepCmd.StdinPipe()
	grepOut, _ := grepCmd.StdoutPipe()
	grepErr, _ := grepCmd.StderrPipe()

	err := grepCmd.Start()
	if err != nil {
		log.Println("Start command result:", err)
		return nil
	}
	p := subProx{grepIn, grepOut, grepErr, grepCmd, cmd}
	subprocList = append(subprocList, &p)
	go handleSubprocConnection(&p, inQ)
	return &p

}



// Write a message to a connection
func writeMessage(c net.Conn, m string) {
	w := bufio.NewWriter(c)
	w.Write([]byte(m))
	w.Flush()
}

// Broadcast messages to all connected clients
func broadcast(Q chan connection) {
	for {
		m := <-Q

		var mess svarmrgo.Message
		_ = json.Unmarshal([]byte(m.raw), &mess)
		processMessage(mess)
		inMessages++
		for _, c := range connList {
			if c != nil && c != m.port {
				go writeMessage(c, m.raw) //FIXME use proper output queues so we can drop misbehaving clients
				outMessages++
			}
		}

		for _, c := range subprocList {
			if c != nil && c != m.handle {
				go c.In.Write([]byte(m.raw)) //FIXME use proper output queues so we can drop misbehaving clients
				outMessages++
			}
		}

		// Send the message "up" to the calling process
		fmt.Println(m.raw)

	}
}

// Process incoming messages
func processMessage(m svarmrgo.Message) []svarmrgo.Message {
	switch m.Selector {
	case "reveal-yourself":
	inQ <- connection{nil, nil, svarmrgo.WireFormat(svarmrgo.Message{Selector: "announce", Arg: "spine"})}
	case "start-module":  //Start a new module, i.e. a sub-process
		go StartSubproc(m.Arg, []string{"pipes"}, []string{AppDirectory, SvarmrDirectory})
	case "debug":  //Debug message
		log.Println(m.Arg)
	case "log":  //Log message
		log.Println(m.Arg)
	case "error":  //Error message
		log.Println(m.Arg)

	}
	return []svarmrgo.Message{}
}

//Read incoming messages and place them on the input queue
func handleConnection(conn net.Conn, Q chan connection) {
	reader := bufio.NewReader(conn)

	for {
		t, err := reader.ReadString('\n')
		if err != nil {
			//fmt.Println("Client disconnected: ", err)
			return
		}
		var m connection = connection{conn, nil, t}
		Q <- m
	}

}

var inQ chan connection

func main() {
	// Find the path to the svarmr directory, using our executable path
	SvarmrDirectory, _ = osext.ExecutableFolder()
	// Consider where we start to be the application directory (where we will find modules)
	AppDirectory, _ = os.Getwd()

	flag.StringVar(&AppDirectory, "appdir", AppDirectory, "Full path to applicaton directory")
	flag.StringVar(&SvarmrDirectory, "svarmrdir", SvarmrDirectory, "Full path to svarmr directory")
	flag.Parse()

	log.Printf("Found svarmr in %v, running application from %v", SvarmrDirectory, AppDirectory)
	inQ = make(chan connection, 200)
	connList = make([]net.Conn, 0)
	//Don't run network sockets from the server anymore, run the network relay module to handle TCP socket clients
	go broadcast(inQ)

	//Start the modules.  Each module is started in a separate process, and communicates with the server over STDIN/STDOUT.  They are listed on the command line, without the .sh or .exe extension
	for _, v := range flag.Args() {
		log.Println("Starting ", v)
		StartSubproc(v, []string{"--svarmrdir", SvarmrDirectory, "--appdir", AppDirectory}, []string{AppDirectory, SvarmrDirectory})
	}

	// Listen for incoming messages from 'above' - i.e. the calling process
	go readMesagesFromStdin(inQ)

	// Notify the user that we started, if there is a notification service
	go func() {
		inQ <- connection{nil, nil, svarmrgo.WireFormat(svarmrgo.Message{Selector: "user-notify", Arg: "Server started"})}
	}()


	for {
		// Send debug ticks every 10 seconds, as a keep-alive
		//inQ <- connection{nil, nil, svarmrgo.WireFormat(svarmrgo.Message{Selector: "debug", Arg: "Server main loop active"})}
		time.Sleep(10.0 * time.Second)
	}
}
